package ai.worlds.vacuum;

import java.util.Vector;

/**
 * A vacuum agent that simply chooses actions at random
 * 
 * @author Jill Zimmerman -- jill.zimmerman@goucher.edu
 *
 */

public class RJVacuumAgent extends VacuumAgent {
	/**
	 * Determine the next action to be performed.
	 */
	public void determineAction() {

		
		Vector p = (Vector) percept;
		if (p.elementAt(1) == "dirt")
			action = "suck";
		else if (p.elementAt(2) == "home") {
			action = stop(p);
			
		} else if (p.elementAt(0) == "bump") {
			action = ramdomSide();
		} else {
			int i = (int) Math.floor(Math.random() * 5);
			switch (i) {
			case 0:
				action = "forward";
				break;
			case 1:
				action = "forward";
				break;
			case 2:
				action = "forward";
				break;
			case 3:
				action = "turn right";
				break;
			case 4:
				action = "turn left";
			}
		}
		System.out.println(action);
	}
	
	private String ramdomSide(){
		int i = (int) Math.floor(Math.random() * 2);
		switch (i) {
		case 0:
			return "turn right";
		default:
			return "turn left";
		}
	}
	
	private String stop(Vector perceptions){
		String acao = null;
		if( score > 100){
			acao = "shut-off";
		}
		else if(perceptions.elementAt(0) == "bump"){
			acao = ramdomSide();
		}
		else{
			acao = "forward";
		}
		return acao;
	}
}