package ai.worlds.vacuum;

import java.util.Vector;

/**
 * A vacuum agent that simply chooses actions at random
 * 
 * @author Jill Zimmerman -- jill.zimmerman@goucher.edu
 *
 */

public class RJModelBasedVacuumAgent extends VacuumAgent {
	/**
	 * Determine the next action to be performed.
	 */

	String lastAction = null;
	String lastPerception = null;

	public void determineAction() {

		Vector p = (Vector) percept;
		if (p.elementAt(1) == "dirt") {
			action = "suck";
			lastPerception = (String) p.elementAt(1);
		} else if (p.elementAt(2) == "home") {
			if (lastAction != null && lastAction.equals("forward")) {
				action = "shut-off";
			} else {
				action = "forward";
			}
			lastPerception = (String) p.elementAt(2);

		} else if (p.elementAt(0) == "bump") {
			if (lastAction.equals("turn right")) {
				action = "turn right";
			}
			else if (lastAction.equals("turn left")) {
				action = "turn left";
			}
			action = ramdomSide();
			lastPerception = (String) p.elementAt(0);
		} else {
			if (lastAction.equals("turn right") || lastAction.equals("turn left")) {
				action = "forward";
			}
			else if (lastAction.equals("turn right")) {
				action = "turn left";
			}
			else if (lastAction.equals("turn left")) {
				action = "turn right";
			}else {
				action = ramdomSide();
			}
			lastPerception = "";
		}
		lastAction = action;
		System.out.println(" acao: " + action + "\tpercepcao: " + lastPerception);
	}

	private String ramdomSide() {
		int i = (int) Math.floor(Math.random() * 2);
		switch (i) {
		case 0:
			return "turn right";
		default:
			return "turn left";
		}
	}

	private String stop(Vector perceptions) {
		String acao = null;
		if (score > 100) {
			action = "shut-off";
		} else if (perceptions.elementAt(0) == "bump") {
			action = ramdomSide();
		}
		return acao;
	}
}